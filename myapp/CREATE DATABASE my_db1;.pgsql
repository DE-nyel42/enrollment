CREATE DATABASE my_db1;

CREATE TABLE student (
StdId int NOT NULL,
FirstName varchar(45) NOT NULL,
LastName varchar(45) DEFAULT NULL,
Email varchar(45) NOT NULL,
PRIMARY KEY (StdId),
UNIQUE (Email)
)

Create Table course (
    CourseID int not null,
    CourseName varchar(45) not null,
    Primary Key (CourseID)
)

Delete table course