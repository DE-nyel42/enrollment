package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

//test case to check admin login
func TestAdminLogin(t *testing.T){
	url := "http://localhost:8081/login"

	// input data
	var jsonStr =[]byte(`{"email":"kuzu@gmail.com", "password":"kuzu"}`)

	req, _:=http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))

	req.Header.Set("Content-Type", "application/json")
	
	// creating client
	client := &http.Client{}

// send api login req
	res, err := client.Do(req)

	// handle error
	if err!= nil{
		panic(err)
	}

	defer res.Body.Close()
	//get data from the response body
	body, _ :=io.ReadAll(res.Body)

	//expected response
	expRes := `{"message":"success"}`
	// compare expected and actual response body
	assert.JSONEq(t, expRes, string(body))
	// compare status code
	assert.Equal(t,http.StatusOK, res.StatusCode)
}

func TestAdminUserNotExist(t *testing.T){
	url := "http://localhost:8081/login"

	// input data
	var jsonStr =[]byte(`{"email":"kuz@gmail.com", "password":"kuz"}`)

	req, _:=http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))

	req.Header.Set("Content-Type", "application/json")
	
	// creating client
	client := &http.Client{}

// send api login req
	res, err := client.Do(req)

	// handle error
	if err!= nil{
		panic(err)
	}

	defer res.Body.Close()
	//get data from the response body
	body, _ :=io.ReadAll(res.Body)

	//expected response
	expRes := `{
		"error": "sql: no rows in result set"
	  }`
	// compare expected and actual response body
	assert.JSONEq(t, expRes, string(body))
	// compare status code
	assert.Equal(t,http.StatusUnauthorized, res.StatusCode)
}