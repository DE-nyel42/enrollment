package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddStudent(t *testing.T){
	url := "http://localhost:8081/student"

	var jsonStr = []byte(`{"stdid":1228, "fname":"Tshering", "lname":"tshomo", "email":"shering@gmail.com"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client:=&http.Client{}
	res,err := client.Do(req)

	if err != nil{
		panic(err)
	}

	defer res.Body.Close()

	body,_:= io.ReadAll(res.Body)
	assert.Equal(t, http.StatusCreated, res.StatusCode )

	expectedRes := `{
		"status": "student added"
	  }`
	
	  assert.JSONEq(t, expectedRes, string(body))
}

func TestGetStudent(t *testing.T){
	c := http.Client{}
	r, _:= c.Get("http://localhost:8081/student/1228")
	body,_:=io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)

	expRes := `{
		"stdid": 1228,
		"fname": "Tshering",
		"lname": "tshomo",
		"email": "shering@gmail.com"
	  }`

	assert.JSONEq(t, expRes, string(body))
}

func TestDeleteStudent(t *testing.T){
	url := "http://localhost:8081/student/1227"
	req, _ := http.NewRequest("DELETE", url, nil)
	client :=&http.Client{}
	res,err:= client.Do(req)
	if err!=nil{
		panic(err)
	}
	defer res.Body.Close()

	body,_:=io.ReadAll(res.Body)
	assert.Equal(t, http.StatusOK, res.StatusCode)

	expRes := `{
		"status": "deleted"
	  }`

	assert.JSONEq(t, expRes, string(body))
}

func TestStudentNotFound(t *testing.T){
	assert := assert.New(t)
	c := http.Client{}
	r, _ := c.Get("http://localhost:8081/student/1002")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"Student not found"}`
	assert.JSONEq(expResp, string(body))
}