package model

import (
	"myapp/dataStore/postgres"
)

type Enroll struct {
	StdId         int64  `json:"stdid"`
	CourseID      string `json:"cid"`
	Date_Enrolled string `json:"date"`
}

const (
	queryEnrollStd = "INSERT INTO enroll(stdid, courseid,date_enrolled) VALUES($1, $2, $3) RETURNING stdid;"
)

func (e *Enroll) EnrollStud() error {
	row := postgres.Db.QueryRow(queryEnrollStd, e.StdId, e.CourseID, e.Date_Enrolled)
	err := row.Scan(&e.StdId)
	return err
}

const queryGetEnoll = "Select stdid, courseid, date_enrolled from enroll where stdid=$1 and courseid=$2"

func (e *Enroll) Get() error {
	return postgres.Db.QueryRow(queryGetEnoll, e.StdId, e.CourseID).Scan(&e.StdId, &e.CourseID, &e.Date_Enrolled)
}

func GetAllEnrolls() ([]Enroll, error) {
	rows, getErr := postgres.Db.Query("Select stdid, courseid, date_enrolled from enroll")
	if getErr != nil {
		return nil, getErr
	}
	enrolls := []Enroll{}

	for rows.Next() {
		var e Enroll
		dbErr := rows.Scan(&e.StdId, &e.CourseID, &e.Date_Enrolled)
		if dbErr != nil {
			return nil, dbErr 
		}
		enrolls = append(enrolls,e)
	}
	rows.Close()
	return enrolls, nil
}

const queryDeleteEnroll = "Delete from enroll where stdid=$1 and courseid=$2 returning stdid"

func (e *Enroll) Delete() error{
	row := postgres.Db.QueryRow(queryDeleteEnroll, e.StdId, e.CourseID )
	err := row.Scan(&e.StdId)
	return err
}